import os
import mod 

print('Welcome to Gaxenu!')
print('Enter help command for Information about Gaxenu')

def information():
    print('show \n   for print elements(modules, requirements)')
    print('info\n   for print infos(modules, requirements)')

#modules = ['wlc']
infos = ['wlc openings wordlist cleaner. Its make cleanly your wordlist files' ]
infos = {
        'modules': 'Little tools for Gaxenu',
        'wlc': 'Openings wordlist cleaner. Its make cleanly your wordlist files'
}

module_helps = {
        'wlc': 'USAGE\nArgs\n   |  Targets  | Output |\n   |-t=t1,t2,t3|-o=o.txt|'

        }
def refresh_module():
     os.system("ls modules > .modules")
     f = open('.modules', 'r')
     modules_list = f.read().split('\n')
     modules_list.pop(-1)
     counter = 0
     for i in modules_list:
         if i == '__pycache__':
             print("removed",modules_list[counter])
             modules_list.pop(counter)
         counter += 1
     counter = 0
     for i in modules_list:
         tmp = ''
         for j in i:
             if not(j == '.'):
                 tmp += j
             else:
                 break
         modules_list[counter] = tmp
         counter += 1
     f.close()
     f = open('./mod.py', 'w')
     for i in modules_list:
         f.write('from modules import '+i+'\n')
     f.write('def run(cmd):')
     for i in modules_list:
         f.write('\n\tif cmd == "'+i +'":\n\t\t'+i+'.run()\n')
     f.close()
     print(modules_list,'\nCompleted\n you can restart tool')

def main():
    while True:
        cmd = input('$')

        if cmd[:4] == 'show':
            if cmd[5:] == 'modules':
                f = open('.modules')
                print(f.read())
                # if len(modules) == 1:
                    # print(modules[0])
                # else:
                    # tmp=''
                    # for i in modules:
                        # tmp += i+', '
                    # print(tmp[:-2])
        if cmd == 'refresh module':
            refresh_module()
        if cmd[:3] == 'use':
            #print(os.system('cat modules.py'))
            mod.run(str(cmd[4:]))
            #if cmd[4:] == 'wlc':
                #wlc.app()
        if cmd[:4] == 'info':
            print(infos[cmd[5:]])
        if cmd == 'help':
            information()
        elif cmd == 'cls':
            os.system('clear')
        elif cmd == 'quit':
            exit()

if __name__ == '__main__':
    main()
