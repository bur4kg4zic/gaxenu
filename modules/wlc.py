module_helps = {
        'wlc': 'USAGE\nArgs\n   |  Targets  | Output |\n   |-t=t1,t2,t3|-o=o.txt|'

        }

def run():
    def start(targets, output):
        content = []
        if not(targets == []):
            for i in targets:
                f = open(i,'r')
                content += f.read().split('\n')
        else:
            print('You have to give us target files and output file!')
        outlog = []
        for i in content:
            if not(outlog.__contains__(i)) and not(i == ''):
                    outlog.append(i)
        f = open(output, 'w')
        for i in outlog:
            f.write(i+'\n')
        f.close()

    while True:
        wlc_cmd = input('=x ')

        empty = False
        for i in wlc_cmd[::-1]:
            if i == ' ':
                wlc_cmd = wlc_cmd[:-1]
            else :
                break
        for i in wlc_cmd:
            if i == ' ':
                empty=True

        if empty:
            multicmd = wlc_cmd.split(' ')
            second_cmd = multicmd[2] 
            if multicmd[0] == 'run':
                if multicmd[1][:2] == '-t':
                    targets = multicmd[1][3:].split(',')
                elif multicmd[1][:2] == '-o':
                    output = multicmd[1][3:]
                if second_cmd[:2] == '-t':
                    targets = multicmd[2][3:].split(',')
                elif second_cmd[:2] == '-o':
                    output = multicmd[2][3:]
                else:
                    print('There is no command like that')
                start(targets, output)

        if wlc_cmd == 'help':
            print(module_helps['wlc'])
        elif wlc_cmd == 'cls':
            os.system('clear')
        elif wlc_cmd == 'quit':
            exit()
